# Script för konvertering av tjänstekontrakt
Här finns script för att konvertera E-Hälsomyndighetens tjänstekontrakt till RIVTA Basic Profile 2.1.

De olika tjänstedomänerna har egna git repositories under http://bitbucket.org/rivta-domains/riv.se.apotekensservice.*

Här beskrivs de olika script som förekommer eller har förekommit:

## CreateRivTaFoldersAndContentFromApSeZip.groovy
Scriptet var tänkt att köras från roten av det uppackade releasepaketet (zip) från E-Hälsomyndigheten

1. Skapade en Subversion-katalogstruktur för varje subdomän, vardera innehållande katalogerna trunk, tags och branches.
2. Skapade katalogerna schemas/interactions och schemas/core_components under trunk
3. Kopierade filerna tillhörande respektive interaktion till underkataloger under schemas/interactions

Detta script är numera borttaget, då denna katalogstruktur redan finns under respektive tjänstedomän i Bitbucket, samt att det inte längre är aktuellt att ersätta ALLA interaktionsfiler i en tjänstedomän.

## Convert_ApSe_to_RIVTA21.groovy
Scriptet var tänkt att köras från roten till den katalogstruktur som ovanstående script skapat

1. Modifierade wsdl-filer, genom att lägga till LogicalAddress och ArgosHeader
2. Modifierade tjänstescheman genom att inleda sökvägen till domänschemat med ../../core_components

Del två i detta script (uppdatera tjänstescheman) är numera avstängt, då vissa tjänstescheman refererar
till flera domänscheman, vilket gör det komplicerat att sätta om alla "namespace prefix". 

Det är heller inte lämpligt att köra scriptet från en rotkatalog, då enbart vissa interaktioner förändras numera, och scriptet inte kan upptäcka wsdl-filer som redan är korrekta (istället läggs idag en ytterligare omgång headers till). Därför bör scriptet köras från katalogen till en specifik interaktion, där wsdl-filen behöver uppdateras.

## create_servicecontract_archives.sh
Scriptet var tänkt att köras från rotkatalogen till en specifik konverterad tjänstedomän.

1. Frågade efter aktuell domänversion
2. Skapade en zip-fil för release.

Detta script är borttaget, då ingen verifiering mot RIVTA gjordes innan zip-filer skapades. Detta görs
numera med ordinarie Python-releasescript istället.

# Arbetsgång vid uppdatering av interaktioner
Här beskrivs den arbetsgång som senast använts vid uppdatering av interaktionsfiler

1. Packa upp releasepaketet från E-Hälsomyndigheten, till valfri katalog
2. Skapa en lokal arbetskopia av den tjänstedomän du ska arbeta med (t.ex. riv.se.apotekensservice.lf). Detta görs via "git clone" från Bitbucket.
3. Kopiera ev. nya domänscheman (t.ex. se.apotekensservice_lf_5.0.xsd) till schemas/core_components.
4. Undersök vilka interaktioner som behöver uppdateras. För varje sådan, gör följande:
    1. Ta bort alla befintliga xsd- och wsdl-filer och ersätt med ny motsvarighet från EHM:s releasepaket
    2. I en kommandoprompt, ställ dig i interaktionens katalog, t.ex. schemas/interactions/LasLFVardsystemInteraction
    3. Kör scriptet "Convert_ApSe_to_RIVTA21.groovy", t.ex. "groovy c:\temp\ehm-scripts\skript\Convert_ApSe_to_RIVTA21.groovy". Detta skall lägga till LogicalAddress och ArgosHeader i wsdl-filen, samt ev. döpa om filnamnet från RIVTABP20 till RIVTABP21.
    4. Öppna tjänsteschemat (xsd-filen) i en texteditor och ändra sökvägarna till domänschemat under import/schemaLocation, så att de inleds med "../../core_components/".
5. Committa ändringarna med din git-klient
6. Skapa en tag med aktuellt versionsnummer, t.ex. "se.apotekensservice_lf_4.0_RC1"
7. Pusha taggen och uppdaterade filer till Bitbucket.

Därefter kan ev. ändringar och ytterligare RC-versioner skapas manuellt på vanligt sätt. Sedan görs en skarp releasetagg och releasepaket enligt ordinarie rutiner.