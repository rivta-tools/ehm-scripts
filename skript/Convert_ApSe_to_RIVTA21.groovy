import groovy.xml.StreamingMarkupBuilder
import groovy.io.FileType

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Script to update Apotekens Service RIVTA-based WSDL-files to full RIVTA (they lack logiocal address header) plus Argos Header
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

import groovy.xml.XmlUtil

// Process all wsdl files recursively found bellow current directory
new File('.').eachFileRecurse(FileType.FILES) {
	file ->
		if (file.path.contains('.git')) {
			return
		} else if (file.path.endsWith('.wsdl')) {
			processWsdlFile file
		} else if (file.path.contains('Responder')) {
			processServiceSchemaFile file
		}
}

def processWsdlFile(File file) {

	println "Processing " + file.path
	def fileName = file.toPath().fileName
	def version = Float.parseFloat( (fileName =~ /\d\.\d/)[0] )
	def argosNS = "urn:riv:inera.se.apotekensservice:argos:1"

	def definitions = new XmlSlurper().parse(file).declareNamespace(
			xs: "http://www.w3.org/2001/XMLSchema",
			wsdl: "http://schemas.xmlsoap.org/wsdl/",
			soap: "http://schemas.xmlsoap.org/wsdl/soap/")


	String targetNamespace = definitions.'@targetNamespace'.text()
	targetNamespace = targetNamespace.replace(":rivtabp20", ":rivtabp21")

	definitions.'@targetNamespace' = targetNamespace

	// Add target  namespace to schema tag (required by BizTalk)
	definitions.'wsdl:types'.'xs:schema'.'@targetNamespace' = targetNamespace

	// Add import for logical address header and argos header elements
	definitions.'wsdl:types'.'xs:schema'.appendNode {
		'xs:import' (schemaLocation : "../../core_components/itintegration_registry_1.0.xsd", namespace : "urn:riv:itintegration:registry:1")
		'xs:import' (schemaLocation : "../../core_components/ArgosHeader_1.0.xsd", namespace : argosNS)
	}

	// Add LogicalAddress and argos header parts to request messages
	definitions.'wsdl:message'.findAll {it.@name.toString().endsWith("Request")}.each {
		it.appendNode {
			'wsdl:part'(name : 'LogicalAddress', element : "itr:LogicalAddress") {
				'wsdl:documentation' ("Orgnr of Apotekens Service AB")
			}
		}
		it.appendNode {
			'wsdl:part'(name : 'ArgosHeader', element : "argos:ArgosHeader") {
				'wsdl:documentation' ("Argos header of Apotekens Service AB. Check documentation regarding mandatory fields for this specific service interaction")
			}
		}
	}

	// Add soap header bindings to all input operation bindings
	definitions.'wsdl:binding'.'wsdl:operation'.each {
		def messageName = "" + it.'@name' + "Request"
		it.'wsdl:input'.appendNode {
			'soap:header'(use: 'literal', message: messageName, part: 'LogicalAddress' )
		}
		it.'wsdl:input'.appendNode {
			'soap:header'(use: 'literal', message: messageName, part: 'ArgosHeader' )
		}
	}

	// Derive service schema namespace from target namespace attribute. Namespace declarations are lost when re-writing the
	// processed WSDL, so we need to derive the namespace from the targetNamespace attribute (which is not lost)
	// Example: from 	urn:riv:se.apotekensservice:or:SkapaDosunderlagVard:1:rivtabp21
	// 			to 		urn:riv:se.apotekensservice:or:SkapaDosunderlagVardResponder:1
	List<String> nsParts = targetNamespace.split(':').toList()
	nsParts.remove(nsParts.size() - 2)
	nsParts.remove(nsParts.size() - 1)
	nsParts[nsParts.size() - 1] += "Responder"
	nsParts.add(version.intValue())

	def serviceSchemaNS = nsParts.join(':')

	// Re-define namespace declarations and over-write the wsdl file with the modifed content
	def smb = new StreamingMarkupBuilder().bind {
		mkp.declareNamespace(
				xs: "http://www.w3.org/2001/XMLSchema",
				wsdl: "http://schemas.xmlsoap.org/wsdl/",
				soap: "http://schemas.xmlsoap.org/wsdl/soap/",
				http: "http://schemas.xmlsoap.org/wsdl/http/",
				spdr: serviceSchemaNS,
				itr: "urn:riv:itintegration:registry:1",
				argos: argosNS,
				'': "${definitions.'@targetNamespace'}"
		)
		mkp.yield definitions
	}

	String newWsdl = XmlUtil.serialize(smb)

	// Update the main documentation part
	newWsdl = newWsdl.replace("Basic Profile 2.0", "Basic Profile 2.1")

	def oldPath = file.path
	def newPath = oldPath.replace("20.wsdl", "21.wsdl")
	println "Saving ${newPath}"
	new File(newPath).write(newWsdl, "UTF-8");

	if (oldPath.endsWith("20.wsdl")) {
		println "Deleting ${oldPath}"
		new File(oldPath).delete()
	}
}

def processServiceSchemaFile(File file) {

	// Disable this transformation for now - interactions with multiple domain schemas makes it difficult to automate
	// (for example when re-setting namespace prefixes) - please update schemaLocations manually instead
	return


	println "Processing " + file.path

	def schema = new XmlSlurper().parse(file).declareNamespace(xs: "http://www.w3.org/2001/XMLSchema")

	def domainSchemaImportNode = schema.'xs:import'[0]

	if (domainSchemaImportNode.'@schemaLocation'.text().startsWith('../../')) {
		println "Skipping file - already converted."
		return
	}

	// Update schemaLocation for existing import of the domain schema, since domain schema location has changed
	def domainSchemaNewLocation = "../../core_components/" + domainSchemaImportNode.'@schemaLocation'
	domainSchemaImportNode.'@schemaLocation' = domainSchemaNewLocation

	// Namespace example: urn:riv:se.apotekensservice:expo:SkapaApotekResponder:4
	def serviceSchemaNS = schema.'@targetNamespace'.text()
	def subDomainName = serviceSchemaNS.split(':')[3]

	// Re-define namespace declarations and over-write the wsdl file with the modifed content
	def smb = new StreamingMarkupBuilder().bind {
		mkp.declareNamespace(
				xs: "http://www.w3.org/2001/XMLSchema",
				(subDomainName) : domainSchemaImportNode.'@namespace'.text(),
				'': serviceSchemaNS
		)
		mkp.yield schema
	}

	String newServiceSchema = XmlUtil.serialize(smb)

	new File(file.path).write(newServiceSchema, "UTF-8");
}
	